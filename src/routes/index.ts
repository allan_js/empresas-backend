import { Router } from 'express';
import usersRouter from './users.routes';
import sessionsRouter from './sessions.routes';
import enterprisesRouter from './enterprises.routes';

const routes = Router();

routes.use('/enterprises', enterprisesRouter);

routes.use('/users', usersRouter);

routes.use('/sessions', sessionsRouter);

export default routes;
