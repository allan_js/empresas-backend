import { Router } from 'express';
import { getRepository } from 'typeorm';

import CreateEnterpriseService from '../services/CreateEnterpriseService';
import Enterprises from '../models/Enterprises';

import ensureAuthenticated from '../middlewares/ensureAuthenticated';

const enterprisesRouter = Router();

enterprisesRouter.use(ensureAuthenticated);

enterprisesRouter.get('/', async (request, response) => {
  const enterprisesRepository = getRepository(Enterprises);
  const appointmens = await enterprisesRepository.find();

  return response.json(appointmens);
});

enterprisesRouter.get('/filter', async (request, response) => {
  const { name, city, country } = request.query;
  const enterprisesRepository = getRepository(Enterprises);
  const appointmens = await enterprisesRepository.find({
    where: [{ enterprise_name: name }, { country }, { city }],
  });

  return response.json(appointmens);
});

enterprisesRouter.post('/', async (request, response) => {
  const {
    email_enterprise,
    facebook,
    twitter,
    linkedin,
    phone,
    own_enterprise,
    enterprise_name,
    photo,
    description,
    city,
    country,
    value,
    share_price,
  } = request.body;

  const createEnterprise = new CreateEnterpriseService();

  const enterprise = await createEnterprise.execute({
    email_enterprise,
    facebook,
    twitter,
    linkedin,
    phone,
    own_enterprise,
    enterprise_name,
    photo,
    description,
    city,
    country,
    value,
    share_price,
  });

  return response.json(enterprise);
});

export default enterprisesRouter;
