import { getRepository } from 'typeorm';

import Enterprise from '../models/Enterprises';

interface Request {
  email_enterprise: string;
  facebook: string;
  twitter: string;
  linkedin: string;
  phone: string;
  own_enterprise: boolean;
  enterprise_name: string;
  photo: string;
  description: string;
  city: string;
  country: string;
  value: number;
  share_price: number;
}

class CreateEnterpriseService {
  public async execute({
    email_enterprise,
    facebook,
    twitter,
    linkedin,
    phone,
    own_enterprise,
    enterprise_name,
    photo,
    description,
    city,
    country,
    value,
    share_price,
  }: Request): Promise<Enterprise> {
    const enterprisesRepository = getRepository(Enterprise);

    const enterprise = enterprisesRepository.create({
      email_enterprise,
      facebook,
      twitter,
      linkedin,
      phone,
      own_enterprise,
      enterprise_name,
      photo,
      description,
      city,
      country,
      value,
      share_price,
    });

    await enterprisesRepository.save(enterprise);

    return enterprise;
  }
}

export default CreateEnterpriseService;
