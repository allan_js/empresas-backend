# README

Estes documento README tem como objetivo fornecer as informações necessárias para realização do projeto Empresas.

### O QUE FAZER ?

- Você deve realizar um fork deste repositório e, ao finalizar, enviar o link do seu repositório para a nossa equipe. Lembre-se, NÃO é necessário criar um Pull Request para isso, nós iremos avaliar e retornar por email o resultado do seu teste.

### ESCOPO DO PROJETO

- Deve ser criada uma API em **NodeJS** ou **Ruby on Rails**.
- A API deve fazer o seguinte:

1. Login e acesso de Usuário já registrado;
2. Para o login usamos padrões **JWT** ou **OAuth 2.0**;
3. Listagem de Empresas
4. Detalhamento de Empresas
5. Filtro de Empresas por nome e tipo

### Informações Importantes

- A API deve funcionar exatamente da mesma forma que a disponibilizada na collection do postman, mais abaixo os acessos a API estarão disponíveis em nosso servidor.

  - Para o login usamos padrões OAuth 2.0. Na resposta de sucesso do login a api retornará 3 custom headers (access-token, client, uid);

  - Para ter acesso as demais APIS precisamos enviar esses 3 custom headers para a API autorizar a requisição;

- Mantenha a mesma estrutura do postman em sua API, ou seja, ela deve ter os mesmo atributos, respostas, rotas e tratamentos, funcionando igual ao nosso exemplo.

- Quando seu código for finalizado e disponibilizado para validarmos, vamos subir em nosso servidor e realizar a integração com o app.

- Independente de onde conseguiu chegar no teste é importante disponibilizar seu fonte para analisarmos.

- É obrigatório utilização de Banco de Dados MySql/PostgreSQL

### Dados para Teste

- Servidor: https://empresas.ioasys.com.br/
- Versão da API: v1
- Usuário de Teste: testeapple@ioasys.com.br
- Senha de Teste : 12341234

### Dicas

- Documentação JWT https://jwt.io/
- Frameworks NodeJS:

  1. https://expressjs.com/pt-br/
  2. https://sailsjs.com/

- Guideline rails http://guides.rubyonrails.org/index.html
- Componente de autenticação https://github.com/lynndylanhurley/devise_token_auth

## Tecnologias utilizadas:

- **Express**
  > É um framework para Node. js.
  
- **ts-node-dev**
  > Versão aprimorada do node-dev que usa o ts-node sob o capô.
  
- **pg**
  > Cliente PostgreSQL sem bloqueio para Node.js. JavaScript puro e ligações libpq nativas opcionais.
  
- **uuidv4**
  > O uuidv4 cria UUIDs da v4.
  
- **TypeScript**
  > É um superconjunto de JavaScript desenvolvido pela Microsoft que adiciona tipagem e alguns outros recursos a linguagem

- **EsLint**
  > É uma ferramenta de análise de código estática para identificar padrões problemáticos encontrados no código JavaScript

- **Prettier**
  > Responsável por formatar o código de acordo com regras cadastradas.
  
- **TypeORM**
  > ORM (Object-Relational Mapper) para Node/TypeScript.
  
- **Docker**
  > É uma plataforma de código aberto para criação de ambientes isolados via container.
  
- **Banco Postgres**
  > Banco relacional Postgres.
  
## Ajustes
- Ajustar o arquivo ormconfig.json com os dados da url, usuario e senha do banco Postgres
- Alterar apenas esses dados:

  ```JSON  
  "type": "postgres",
  "host": "192.168.99.100",
  "port": 5434,
  "username": "postgres",
  "password": "ioasys",
  "database": "ioasys",
  ```
